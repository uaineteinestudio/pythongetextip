#!/usr/bin/python
from urllib.request import urlopen

def byteToString(input):
    return input.decode("utf-8") 

def getip():
    ext_ip = urlopen('https://ident.me').read()
    return byteToString(ext_ip)#is ASCII make it UTF


#TEST IT BY RUNNING AS MODULE
#----------------------------
#Remove commenting out below-
#
#strIP = getip()
#print(strIP)
